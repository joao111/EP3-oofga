class AddProductToProductVars < ActiveRecord::Migration[5.2]
  def change
    add_reference :product_vars, :product, foreign_key: true, null: false
  end
end
