Rails.application.routes.draw do

  devise_for :users, path: '', path_names: {sign_in: 'login', sign_out: 'logout', sign_up: 'register'}
  root to: 'categories#index'

  resources :categories, only: [:index] do
    resources :products, only: [:index]
  end
  get '/cart', to: 'order_items#index'
  resources :order_items, path: '/cart/items'

  get '/cart/checkout', to: 'orders#new', ad: :checkout
  patch '/cart/checkout', to: 'orders#create'
  
end
